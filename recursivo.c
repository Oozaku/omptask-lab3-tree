#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef struct st_list {
    struct st_node *element;
    struct st_list *next;
} list_node;

typedef struct st_node {
    long int id;
    long int parent;
    int key;
    list_node *children;
} node;

typedef struct st_queue {
    node **array;
    long int head;
    long int tail;
    long int n;
} queue;

queue* init_queue(long int n){
    queue *q = (queue *) malloc(sizeof(queue));
    q->head = 0;
    q->tail = 0;
    q->n = n;
    q->array = (node **) malloc(sizeof(node*) * n);
    return q;
}

void enqueue(queue *q, node *x){
    (q->array)[(q->tail)++] = x;
    if (q->tail == q->n)
        q->tail = 0;
}

node* dequeue(queue *q){
    if (q->head == q->tail)
        return NULL;
    node *ret = (q->array)[(q->head)++];
    if (q->head == q->n)
        q->head = 0;
    return ret;
}

void print_node(node* x){
    printf("node:%ld key:%d\n", x->id, x->key);
    printf("parent: %ld\nchildren:", x->parent);
    list_node *current = x->children;
    while(current){
        printf("%ld ", (current->element)->id);
        current = current->next;
    }
    printf("\n");
}

void print_map(node **map, long int n){
    for(long int i = 0; i < n; i++){
        if (map[i])
            print_node(map[i]);
    }
}

void add_child(node *parent, node *child){

    list_node *head = parent->children;
    list_node *new = (list_node *) malloc(sizeof(list_node));
    new->element = child;
    new->next = head;
    parent->children = new;
}

void free_map(node **map, int n);
void free_children(list_node *head);

long int recursao(node *root, long int i){
    list_node *children = root->children;
    long int maior = 0;
    if (children == NULL)
        return root->key;
    omp_lock_t write;
    omp_init_lock(&write);
#   pragma omp parallel default(none) \
    shared(maior,children,i,write)
    {
#       pragma omp single nowait
        {
            while (children != NULL){
                long int percurso;
                list_node *copy = children;
#               pragma omp task final(i > 0) \
                firstprivate(copy) private(percurso)
                {
                    percurso = recursao(copy->element,i+1);
                    omp_set_lock(&write);
                    if (percurso > maior)
                        maior = percurso;
                    omp_unset_lock(&write);
                }
                children = children->next;
            }
        }
    }

    return root->key + maior;
}

int main(){

    /* Part 1: Input and building the tree */
    long int n;
    scanf("%ld", &n);

    if (!n) {
        printf("0\n");
        return 0;
    }

    // allocating a map to store the nodes
    // sizeof(node *) instead of sizeof(node)
    node **map = (node**) malloc(sizeof(node *) * n);

    // reading and creating the nodes
    int key;
    for(long int i = 0; i < n; i++){
        scanf("%d", &key);
        node *new = (node*) malloc(sizeof(node));
        new->id = i;
        new->key = key;
        new->parent = -1;
        new->children = NULL;
        // print_node(new);
        map[i] = new;                       // adding the new node to the map
    }

    // reading the children of each node
    long int id_child, id_parent;
    int n_child;
    for(long int i = 0; i < n; i++){
        scanf("%ld", &id_parent);
        node *current = map[id_parent];
        node *child = NULL;

        scanf("%d", &n_child);
        for(int j = 0; j < n_child; j++){
            scanf("%ld", &id_child);
            child = map[id_child];
            child->parent = id_parent;
            add_child(current, child);       // adding the child address in the parent
        }
    }

    // print_map(map, n);               // debug

    // finding the root
    node *root, *current;
    for(long int i = 0; i < n; i++){
        current = map[i];
        if (current && current->parent == -1) {
            root = current;
            break;
        }
    }
    // print_node(root);              // debug

    /* Part 2: computing the longest path from the root */

    double t_start = omp_get_wtime();

    long int longest_path= recursao(root,0);

    double t_end = omp_get_wtime();

    /* Part 3: Output */

    printf("%ld\n", longest_path);

    printf("%lf\n", t_end - t_start);                 // COMMENT THIS!

    free_map(map,n);

    return 0;
} // main


void free_map(node **map, int n){
    int i;
    for(i=0;  i<n; i++){
        free_children(map[i]->children);
        free(map[i]);
    }
}

void free_children(list_node *head){
    list_node *aux;
    while(head != NULL){
        aux = head;
        head = head->next;
        free(aux);
    }
}
